package site.zsxeee.work.spring.oa.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import site.zsxeee.work.spring.oa.database.entity.User

interface UserRepository:JpaRepository<User, Long>{
    fun findByUserName(userName:String):User?
}