package site.zsxeee.work.spring.oa.controller

import org.apache.shiro.SecurityUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import site.zsxeee.work.spring.oa.database.entity.Dept
import site.zsxeee.work.spring.oa.database.entity.User
import site.zsxeee.work.spring.oa.database.repository.DeptRepository
import site.zsxeee.work.spring.oa.database.repository.JobRepository
import site.zsxeee.work.spring.oa.database.repository.UserRepository
import site.zsxeee.work.spring.oa.util.AlertInfo
import site.zsxeee.work.spring.oa.util.MVFactory
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletResponse

@Controller
class HomeController {
    @Autowired
    private lateinit var deptRepository: DeptRepository
    @Autowired
    private lateinit var jobRepository: JobRepository
    @Autowired
    private lateinit var userRepository: UserRepository

    @RequestMapping("/", method = [RequestMethod.GET])
    fun index(
            @RequestParam status: String?,
            @RequestParam action: String?
    ) = MVFactory()
            .setTargetViewName("index")
            .setPageTitle("主页")
            .addObject("deptList", deptRepository.findAll())
            .addObject("jobList", jobRepository.findAll())
            .make()

    @RequestMapping("/", method = [RequestMethod.POST])
    fun saveProfile(
            @RequestParam nickName: String,
            @RequestParam gender: Boolean?,
            @RequestParam birth: String?,
            @RequestParam email: String,
            @RequestParam tel: String,
            @RequestParam dept: Long,
            @RequestParam job: String,
            response:HttpServletResponse
    ): ModelAndView {
        val mv = MVFactory()
                .setTargetViewName("index")
                .setPageTitle("主页")
                .addObject("deptList", deptRepository.findAll())
                .addObject("jobList", jobRepository.findAll())
        try {
            userRepository.save((SecurityUtils.getSubject().principal as User).also {
                it.nickName = nickName
                it.gender = gender
                it.birth = SimpleDateFormat("yyyy-MM-dd").parse(birth)
                it.email = email
                it.tel = tel
                it.dept = deptRepository.findById(dept).get()
                it.job = jobRepository.findById(job).get()
            })
        } catch (e: Exception) {
            return mv.setAlert(AlertInfo(
                    AlertInfo.AlertLevel.ERROR,
                    "修改失败！"
            ))
                    .make()
        }
        return mv.setAlert(AlertInfo(
                AlertInfo.AlertLevel.ERROR,
                "修改成功！"
        ))
                .make()
    }
}