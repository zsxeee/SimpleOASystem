package site.zsxeee.work.spring.oa

import nz.net.ultraq.thymeleaf.LayoutDialect
import org.apache.shiro.crypto.SecureRandomNumberGenerator
import org.apache.shiro.crypto.hash.Sha256Hash
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition
import org.springframework.beans.factory.annotation.Autowired
import site.zsxeee.work.spring.oa.database.entity.User
import site.zsxeee.work.spring.oa.database.repository.UserRepository
import java.util.*


@SpringBootApplication
class OaApplication

fun main(args: Array<String>) {
    try {
        runApplication<OaApplication>(*args)
    } catch (e: Exception) {
        print("--------------------------------\n" +
                "请先创建并连接数据库\n" +
                "--------------------------------")
    }
}

//Thymeleaf Layout Dialect Bean
@Bean
fun layoutDialect(): LayoutDialect = LayoutDialect(null)