package site.zsxeee.work.spring.oa.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import site.zsxeee.work.spring.oa.database.entity.Job

interface JobRepository:JpaRepository<Job, String> {
}