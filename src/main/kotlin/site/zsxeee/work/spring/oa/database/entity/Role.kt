package site.zsxeee.work.spring.oa.database.entity

import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "role")
class Role {
    @Id
    var id: Long = 0

    @Column(nullable = false)
    var name: String = ""

    @Column
    var comment: String = ""

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_user")
    var users: Set<User> = HashSet()

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn
    var permissions:Set<Permission> = HashSet()

    //log
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    var modifierUser: User = User()

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    var modifyDate: Date = Date()

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    var creatorUser: User = User()

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    var createDate: Date = Date()
}