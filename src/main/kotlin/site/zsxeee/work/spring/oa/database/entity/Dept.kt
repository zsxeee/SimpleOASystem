package site.zsxeee.work.spring.oa.database.entity

import org.hibernate.annotations.JoinColumnOrFormula
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "dept")
class Dept {
    @Id
    @GeneratedValue
    var id:Long = 0

    @Column(nullable = false)
    var name:String = ""

    @Column
    var comment:String = ""
}