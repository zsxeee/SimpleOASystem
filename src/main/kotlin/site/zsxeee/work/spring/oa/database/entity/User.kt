package site.zsxeee.work.spring.oa.database.entity

import org.springframework.data.jpa.domain.AbstractPersistable_.id
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "user")
class User {
    @Id
    @GeneratedValue
    var uid: Long = 0

    @Column(unique = true, updatable = false)
    var userName: String = ""

    @Column(unique = true)
    var nickName: String = ""

    @Column
    var password: String = ""

    @Column
    var salt: String = ""

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    var created: Date = Date()

    //权限资料
    @ManyToMany(fetch = FetchType.LAZY,targetEntity = Role::class, mappedBy = "users")
    var roles:Set<Role> = HashSet()

    //职位资料
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = ForeignKey())
    var dept:Dept? = null

    @ManyToOne(fetch = FetchType.LAZY)
    var job: Job? = null

    //个性化资料
    @Column
    var gender: Boolean? = null

    @Temporal(TemporalType.DATE)
    var birth: Date? = null

    @Column
    var email: String = "example@mail.com"

    @Column
    var tel: String = "000-00000000"

    @Column
    var avatar: String = "/images/avatars/0.jpg"
}