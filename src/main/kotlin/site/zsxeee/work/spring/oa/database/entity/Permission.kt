package site.zsxeee.work.spring.oa.database.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "permission")
class Permission {
    @Id
    @GeneratedValue
    var id:Long = 0

    //log
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    var creatorUser:User = User()

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    var createDate: Date = Date()
}