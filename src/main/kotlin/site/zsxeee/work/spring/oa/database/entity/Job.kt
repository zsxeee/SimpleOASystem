package site.zsxeee.work.spring.oa.database.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "job")
class Job {
    @Id
    @Column(length = 100)
    var code:String = ""

    @Column(nullable = false)
    var name:String = ""

    @Column
    var summary:String = ""
}