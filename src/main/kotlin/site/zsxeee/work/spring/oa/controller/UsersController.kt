package site.zsxeee.work.spring.oa.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import site.zsxeee.work.spring.oa.database.repository.UserRepository
import site.zsxeee.work.spring.oa.util.MVFactory

@Controller
class UsersController {
    @Autowired
    private lateinit var userRepository: UserRepository

    @RequestMapping("/users", method = [RequestMethod.GET])
    fun getUsers() = MVFactory()
            .setTargetViewName("users")
            .setPageTitle("用户管理")
            .addObject("userList", userRepository.findAll())
            .addObject("initTable", true)
            .make()
}