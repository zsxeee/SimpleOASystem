package site.zsxeee.work.spring.oa.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import site.zsxeee.work.spring.oa.database.entity.Job
import site.zsxeee.work.spring.oa.database.repository.JobRepository
import site.zsxeee.work.spring.oa.util.AlertInfo
import site.zsxeee.work.spring.oa.util.MVFactory
import javax.servlet.http.HttpServletResponse

@Controller
class JobsController {
    @Autowired
    private lateinit var jobRepository: JobRepository

    @RequestMapping("/jobs")
    fun getJobs(
            @RequestParam status: String?,
            @RequestParam action: String?
    ) = MVFactory()
            .setTargetViewName("jobs")
            .setPageTitle("职位管理")
            .addObject("jobList", jobRepository.findAll())
            .addObject("initTable", true)
            .apply {
                if (status != null) {
                    setAlert(AlertInfo(
                            when (status) {
                                "success" -> AlertInfo.AlertLevel.SUCCESS
                                else -> AlertInfo.AlertLevel.ERROR
                            },
                            when(action){
                                "add"->"添加"
                                "edit"->"修改"
                                "del"->"删除"
                                else->"操作"
                            } + when (status) {
                                "success" -> "成功"
                                else -> "失败"
                            }
                    ))
                }
            }
            .make()

    @RequestMapping("/jobs/add", method = [RequestMethod.POST])
    fun addJob(@RequestParam code: String, @RequestParam name: String, @RequestParam summary: String, response: HttpServletResponse) {
        try {
            jobRepository.save(Job().also {
                it.code = code
                it.name = name
                it.summary = summary
            })
        } catch (e: Exception) {
            response.sendRedirect("/jobs?status=fail&action=add")
        }
        response.sendRedirect("/jobs?status=success&action=add")
    }

    @RequestMapping("/jobs/del", method = [RequestMethod.GET])
    fun delJob(@RequestParam code: String, response: HttpServletResponse) {
        try {
            jobRepository.deleteById(code)
        } catch (e: Exception) {
            response.sendRedirect("/jobs?status=fail&action=del")
        }
        response.sendRedirect("/jobs?status=success&action=del")
    }
}