package site.zsxeee.work.spring.oa.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import site.zsxeee.work.spring.oa.database.entity.Dept

interface DeptRepository:JpaRepository<Dept, Long>{
}