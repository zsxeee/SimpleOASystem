package site.zsxeee.work.spring.oa.util

import org.apache.shiro.SecurityUtils
import org.springframework.web.servlet.ModelAndView
import site.zsxeee.work.spring.oa.database.entity.User

class MVFactory {
    private var pageInfo = PageInfo("")
    private var alertInfo:AlertInfo? = null
    private val objectMap = HashMap<String, Any?>()
    private var targetViewName = ""

    fun setPageTitle(title: String): MVFactory {
        this.pageInfo.title = title
        return this
    }

    fun setAlert(alertInfo: AlertInfo): MVFactory {
        this.alertInfo = alertInfo
        return this
    }

    fun addObject(key:String, value:Any?): MVFactory {
        this.objectMap[key] = value
        return this
    }

    fun setTargetViewName(viewName:String): MVFactory {
        this.targetViewName = viewName
        return this
    }

    fun make() = ModelAndView().also { mv ->
        mv.viewName = targetViewName
        mv.addObject("pageInfo", pageInfo)
        mv.addObject("alertInfo", alertInfo)
        val subject = SecurityUtils.getSubject().takeIf { it.isAuthenticated }
        mv.addObject("userInfo", subject?.principal as User?)
        mv.addAllObjects(objectMap)
    }
}

class PageInfo(var title: String)

class AlertInfo(
        var level: AlertLevel = AlertLevel.INFO,
        var msg: String = "",
        var detail: String = ""
) {
    enum class AlertLevel(
            val colorClass:String,
            val iconClass:String
    ) {
        SUCCESS("alert-success", "fa-check"),
        INFO("alert-info", "fa-exclamation-circle"),
        WARNING("warning", "fa-exclamation-triangle"),
        ERROR("alert-danger", "fa-times-circle")
    }
}