package site.zsxeee.work.spring.oa.util

import java.awt.Color
import java.awt.Font
import java.awt.image.BufferedImage
import java.util.*


class Validator(width: Int = 90, height: Int = 20, codeCount: Int = 4) {
    private val xx = 15
    private val fontHeight = 18
    private val codeY = 16
    private val codeSequence = charArrayOf('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

    var code:String
    var codePic:BufferedImage
    init {
        // 定义图像buffer
        val buffImg = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        // Graphics2D gd = buffImg.createGraphics();
        // Graphics2D gd = (Graphics2D) buffImg.getGraphics();
        val gd = buffImg.graphics
        // 创建一个随机数生成器类
        val random = Random()
        // 将图像填充为白色
        gd.color = Color.WHITE
        gd.fillRect(0, 0, width, height)

        // 创建字体，字体的大小应该根据图片的高度来定。
        val font = Font("Fixedsys", Font.BOLD, fontHeight)
        // 设置字体。
        gd.font = font

        // 画边框。
        gd.color = Color.BLACK
        gd.drawRect(0, 0, width - 1, height - 1)

        // 随机产生40条干扰线，使图象中的认证码不易被其它程序探测到。
        gd.color = Color.BLACK
        for (i in 0..29) {
            val x = random.nextInt(width)
            val y = random.nextInt(height)
            val xl = random.nextInt(12)
            val yl = random.nextInt(12)
            gd.drawLine(x, y, x + xl, y + yl)
        }

        // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
        val randomCode = StringBuffer()
        var red: Int
        var green: Int
        var blue: Int

        // 随机产生codeCount数字的验证码。
        for (i in 0 until codeCount) {
            // 得到随机产生的验证码数字。
            val code = codeSequence[random.nextInt(36)].toString()
            // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
            red = random.nextInt(255)
            green = random.nextInt(255)
            blue = random.nextInt(255)

            // 用随机产生的颜色将验证码绘制到图像中。
            gd.color = Color(red, green, blue)
            gd.drawString(code, (i + 1) * xx, codeY)

            // 将产生的四个随机数组合在一起。
            randomCode.append(code)
        }
        //存放验证码
        this.code = randomCode.toString()
        //存放生成的验证码BufferedImage对象
        this.codePic = buffImg
    }
}