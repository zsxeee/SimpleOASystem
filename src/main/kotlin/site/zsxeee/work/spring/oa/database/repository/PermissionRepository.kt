package site.zsxeee.work.spring.oa.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import site.zsxeee.work.spring.oa.database.entity.Permission

interface PermissionRepository:JpaRepository<Permission, Long> {
}