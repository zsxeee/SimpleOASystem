package site.zsxeee.work.spring.oa.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import site.zsxeee.work.spring.oa.database.repository.RoleRepository
import site.zsxeee.work.spring.oa.util.MVFactory

@Controller
class RolesController {
    @Autowired
    private lateinit var roleRepository: RoleRepository

    @RequestMapping("/roles", method = [RequestMethod.GET])
    fun getUsers() = MVFactory()
            .setTargetViewName("roles")
            .setPageTitle("角色管理")
            .addObject("roleList", roleRepository.findAll())
            .addObject("initTable", true)
            .make()
}