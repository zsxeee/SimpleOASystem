package site.zsxeee.work.spring.oa.shiro

import org.apache.shiro.mgt.SecurityManager
import org.apache.shiro.realm.Realm
import org.apache.shiro.spring.web.ShiroFilterFactoryBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class ShiroConfiguration {
    @Bean
    fun realm(): Realm = UserRealm()

    @Bean
    fun shiroFilterFactoryBean(securityManager: SecurityManager): ShiroFilterFactoryBean {
        val shiroFilterFactoryBean = ShiroFilterFactoryBean()
        shiroFilterFactoryBean.securityManager = securityManager
        shiroFilterFactoryBean.loginUrl = "/login"

        // 配置不会被拦截的链接 顺序判断

        // 登录成功后要跳转的链接
        shiroFilterFactoryBean.successUrl = "/"

        shiroFilterFactoryBean.filterChainDefinitionMap = LinkedHashMap<String, String>().apply {
            put("/scripts/**", "anon")
            put("/styles/**", "anon")
            put("/images/**", "anon")
            put("/favicon.ico", "anon")
            put("/register", "anon")
            put("/forgot", "anon")
            put("/logout", "logout")
            put("/**", "authc")
        }

        return shiroFilterFactoryBean
    }
}