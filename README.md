# 简易OA系统

> 课程期末个人项目

使用`Kotlin`编写的，基于`Spring boot`、`JPA`的简易OA系统。因时长问题，前端页面使用第三方模板[Shards Dashboard](https://github.com/designrevision/shards-dashboard)修改而来。

## 运行方法
- 创建一个数据库(utf8_general_ci)，并将相关信息填入`src/main/resources/application.properties`中。
- 运行`site.zsxeee.work.spring.oa.OaApplication`类后使用浏览器浏览本地8080端口即可。

## 可用栏目
- 个人资料
- 用户管理
- 角色管理
- 部门管理
- 职位管理
